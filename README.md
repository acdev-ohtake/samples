# README #

This README would normally document whatever steps are necessary to get your application up and running.

### casperjs-sample ###

* casperjsで自動キャプチャするサンプル

### 使い方 ###

* list-new.csvとlist-src.csvを使って対象URLをキャプチャします。
* imagek-diff.phpで画像の差分を生成して、result.txtに変化量を出力します。
* run.shがバッチになっています。

### 補足 ###
下記の作業が事前に必要です。

* ImageMagickインストール
* php pecl imagemagickのライブラリ
* PhantomJS,CasperJSインストール
* composer.jsonで依存ファイルのインストール