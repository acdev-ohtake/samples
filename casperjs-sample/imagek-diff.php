<?php
require_once('./vendor/autoload.php');

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;


function image_diff($path1, $path2, $path3){
    $image1 = new Imagick($path1);
    $image1->setImageColorspace(imagick::COLORSPACE_CMYK);//カラースペースをCMYKに設定
    $image2 = new Imagick($path2);
    $image2->setImageColorspace(imagick::COLORSPACE_CMYK);//同じくCMYKに設定
    $image1->compositeImage($image2, imagick::COMPOSITE_DIFFERENCE, 0, 0 );//二者の差分をとる
    $image1->negateImage(0);//差分画像の色を反転させる
    $image1->compositeImage($image1, imagick::COMPOSITE_MULTIPLY, 0, 0 );//差分画像を自乗して、変化を際立たせる
    $image1->writeImage($path3);//diff画像を書き出す
}


$path1 = "/screenshot/org-pc/";//比較したい画像a
$path2 = "/screenshot/new-pc/";//比較したい画像b
$path3 = "/screenshot/diff/";//出力先の画像diff

$finder = new Finder();
$finder->files()->in(__DIR__.$path1);

echo 'filename,rate,colors'."\r\n";
foreach ($finder as $file) {
    // Dump the absolute path
    $b = $file->getBasename(".".$file->getExtension());

    $img1 = $file->getRealPath();
    $img2 = __DIR__.$path2.$file->getFilename();
    $diff1 = __DIR__.$path3.$b.'_diff.png';

    image_diff($img1, $img2 , $diff1);//関数を実行する


    $i1 = new imagick($img1);
    $i2 = new imagick($img2);

    $resize_width = $i1->getImageWidth();
    $resize_height = $i1->getImageHeight();

    $i2->resizeImage($resize_width, $resize_height, Imagick::FILTER_LANCZOS, 1);

    $i3 = new imagick($diff1);
    try{
    //$result = $i3->identifyFormat("[%mean]");
    $colors = $i3->getImageColors();

    list($result,$rate) = $i1->compareImages($i2, Imagick::METRIC_MEANSQUAREERROR);
    echo $file->getFilename().','.$rate.','.$colors."\r\n";

    }catch(Exception $e){
    echo $e->getMessage()."\r\n";

    }


}


